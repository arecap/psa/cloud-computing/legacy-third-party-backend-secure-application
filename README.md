# Legacy Third-Party Backend Secure Application
Implements an oauth2 client and an oauth2 resource server jwt to secure http access.
This implementation follows https://www.baeldung.com/spring-security-oauth-auth-server

This can be use in order that a legacy backend web application can be integrated into One Identity applications distribution systems compliant.



