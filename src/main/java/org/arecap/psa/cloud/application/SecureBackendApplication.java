package org.arecap.psa.cloud.application;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecureBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecureBackendApplication.class, args);
    }

}
