package org.arecap.psa.cloud.application.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

    @GetMapping("/items")
    public String[] getItems() {
        return new String[]{"Item 1", "Item 2", "Item 3"};
    }
}